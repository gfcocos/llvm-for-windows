LLVM for Windows
================

--------------------------------------------------------------------------------

    :::text
                                                                         ░
                                                                       ░░
                                                                      ░░   ░▒▒▓▒░
                 ░                                                   ░   ░▓███████▓
                   ░░                                               ░░ ▒▓████▓█████▒
                     ░░                                             ▒ ▓███▓▓▓▓▓█▓██▓
          ░░▒▒░▒░      ░░                                          ▒▓▒▒░▓██▓▓▓▓▓▓▓█▓
     ▒▓▓████████▓▓▒▒     ▒░                                         ▒█▓▓░░▓█▓▓▓▓▓███
    ░████████████████▓▓░  ▒▒                ▒░▒░  ░                 ▒█▓▓█▓░▓█▓▓▓▓▓██
     ███▓█▓█▓▓▓▓▓▓▓▓▓███▓▒ ▒░             ▒▓▒░░░▒▓▒▒▒▒▒             ░█▓▓▓█▓▒▓█▓█▓███
     ██▓█▓█▓▓▓▓▓▓▓▓▓▓▓█▓▓▓▓░▓▓           ▓▓  ░░░░░▒▒▒▒▒▒▒           ░▓█▓▓▓▓▓▒▓█▓███▓
     ███▓█▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▓▓▓▓░          ▓█▒          ░▒▒▓▒           ██▓▓▓▓█▓▒▓█▓██▓
     ██▓█▓█▓▓▓▓▓▓▓█▓▒░▒▓█▓▓█▓░          ░▓█░           ░▒▓▒          ██▓▓▓▓▓█▓▒████▒
     ▓██▓█▓▓▓▓▓▓▓█▓░▒▓█▓▓▓▓▓▓░           ░▒░            ▒█▒▒        ░██▓▓▓▓▓▓█▓▓███░
     ░███▓█▓▓▓█▓█▓░▒█▓▓▓▓▓▓▓█▒          ▒▓▓░ ░░          ▓▓░░       ▒██▓▓▓▓▓███░███
      ██▓█▓█▓█▓██░▓█▓▓▓▓▓▓▓▓██░         ▓▓▒▒▒▓▓▓         ▓█▒▒       ▓█▓▓▓▓▓▓██▒ ▓█▓
      ▓██▓█▓█▓██░▒█▓▓▓▓▓▓▓▓▓▓█▓░        ▓▒▒▒▒▓██▓        ▒█▒▒      ░███▓█▓█▓██  ▒█▒
      ░███▓█▓██▒░███▓▓▓▓▓▓▓▓▓██▓░       ░▒▒░  ▒▒░        ▒█▓░      ▒██▓█▓█▓██▒   █
       ████▓███░███▓█▓█▓▓▓▓▓▓▓██▓          ▓    ▒▓       ▓█▓░    ▒ ▓███▓█▓███    ░
       ░██████░▒████▓█▓▓▓█▓█▓████  ▒       ██  ░        ▒▓▓▓░    ▒▒▒██▓█▓███
        ██████  ▓██████▓█▓█▓█▓██▓ ░░░      ▒▓          ░▓▓█▓    ▒▒█▒███████░
         ████▓   ░██████▓█▓█▓███░▒▓▓▓░                ░█▓▒█▒ ░▒▒█▓▓▓▒█████▒
         ▓███░     ▒███████▓███▓ █▓▒██▓▒▒            ░▓▓▓▒▓   ░██▓██▒████▒
          ███        ▒█████████▓░██▓█████▓▒▒░░   ▒▓▓▒▓▓▒▒▒░ ░▓██████▓███░
          ░█▓          ▒███████▓▓██▓█████████▒░░░░░░▒▓▒░░ ░▓██▓▒   ▓███
           █▓             ▓████▓██▒   ░▓██████▓▓▒▒▒▒▒▒░░ ░▓█▒       ░▒
            ▒               ▒▓██▓         ▓█▓▓▓▒▓▓▓░ ░▒▒▒▓▓
                               ░    ▓▓▒▒   ░▓▒▓▓▒▓▒░░  ▓▓▒
                                    █▓▓▓▒░▒░▒▓▒▓▓▒░░░░▒▒
                                    ░▒░░ ▒▓░░▓▓▒▒░ ░▒▒░
                                      ░  ▒█ ░▒▓▒▒▒▓▓▒░▒▓▒░
                   ▒▒▓▓▓▒▒▒▒░░         ░░░▒▓▓▓▒▒▒▓▒░░  ▒░▒
                  ▒▒▒░     ░▒▒▒▒░   ░▒▒▒▒▒▓▓░    ░      ░▒░
                 ▒▓░            ▒▓▒▓▓▒▒▒▒░              ░░
                 ░▓▓░          ░▒▒▒▒▒▒░
                  ░▓▓▓▓▒▒▒▒▒▒▒▒▒▒░    ░▒▒░
                    ░░▒▒▒▒▒▒░░           ░▒▒░
                                             ░░░░░░░░░

Table of Contents
-----------------

--------------------------------------------------------------------------------

  - [Platforms](#markdown-header-platforms)
  - [Architectures](#markdown-header-architectures)
  - [Downloads](#markdown-header-downloads)

Platforms
---------

--------------------------------------------------------------------------------

  - Windows 2000;
  - Windows XP;
  - Windows Vista;
  - Windows 7;
  - Windows 8.

Architectures
-------------

--------------------------------------------------------------------------------

  - x86, x86-32, x32, i686;
  - x64, x86-64, amd64.

Downloads
---------

--------------------------------------------------------------------------------

**NOTE:** Under Windows, LLVM/Clang registers which toolchain was used to build
it, and that affects its behavior in future. For example, when LLVM/Clang knows
that it was built with MinGW/MinGW-w64, it will search for standard include
paths according to the conventional directory structure of MinGW/MinGW-w64. This
allows LLVM/Clang to rely on MinGW/MinGW-w64 standard library. In other words,
generally speaking, one could substitute `gcc.exe` and `g++.exe` with
`clang.exe` and `clang++.exe` respectively, and use LLVM/Clang as compiler and
code generator for Windows. Why? Because LLVM/Clang does not have mature
standard library support on Windows yet. Beware, that all of that is
experimental, and I've listed this information merely to avoid confusion about
why MinGW-w64 is listed in downloads. So, now you know that it's just to inform
you that LLVM/Clang was actually built with MinGW-w64, and if you ever wanted to
try the trick described above, you could have downloaded the respective
MinGW-w64 toolchain. For most users this is irrelevant, and if you are
downloading LLVM/Clang for other purposes, then all you have to pay attention to
is the targeted architecture of the toolchain that was used to build LLVM/Clang.

  - [MinGW-w64/4.8.1/x86/POSIX/SJLJ][]:
      - **[3.4][Downloads/LLVM/3.4/MinGW-w64/4.8.1/x86/POSIX/SJLJ]**.

  - [MinGW-w64/4.8.1/x64/POSIX/SJLJ][]:
      - **[3.4][Downloads/LLVM/3.4/MinGW-w64/4.8.1/x64/POSIX/SJLJ]**.

[MinGW-w64/4.8.1/x86/POSIX/SJLJ]: http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/4.8.1/threads-posix/sjlj/
[MinGW-w64/4.8.1/x64/POSIX/SJLJ]: http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/4.8.1/threads-posix/sjlj/

[Downloads/LLVM/3.4/MinGW-w64/4.8.1/x86/POSIX/SJLJ]: /Haroogan/llvm-for-windows/downloads/llvm-3.4-mingw-w64-4.8.1-x86-posix-sjlj.zip
[Downloads/LLVM/3.4/MinGW-w64/4.8.1/x64/POSIX/SJLJ]: /Haroogan/llvm-for-windows/downloads/llvm-3.4-mingw-w64-4.8.1-x64-posix-sjlj.zip
