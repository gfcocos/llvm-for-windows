#!/usr/bin/env python
# encoding: utf-8

from argparse import ArgumentParser

import os
import subprocess
import tempfile


def main():
  parser = ArgumentParser()

  parser.add_argument('-source-dir',     required = True)
  parser.add_argument('-build-dir',      required = False,
                                         default  = '')
  parser.add_argument('-install-prefix', required = True)
  parser.add_argument('-toolchain',      required = True,
                                         choices  = ['gcc', 'mingw'])
  parser.add_argument('-variant',        required = True,
                                         choices  = ['release', 'debug'])
  parser.add_argument('-c-flags',        required = False,
                                         default  = '')
  parser.add_argument('-cxx-flags',      required = False,
                                         default  = '')
  parser.add_argument('-link-flags',     required = False,
                                         default  = '')
  parser.add_argument('-verbose',        action   = 'store_true',
                                         default  = False)

  args = parser.parse_args()

  source_dir = os.path.realpath(args.source_dir)

  if args.build_dir:
    build_dir = os.path.realpath(args.build_dir)
  else:
    build_dir = tempfile.mkdtemp('.d', 'llvm-build-')

  cmake_install_prefix = os.path.realpath(args.install_prefix)

  if not os.path.isdir(cmake_install_prefix):
    os.makedirs(cmake_install_prefix)

  cmake_build_type    = args.variant.capitalize()
  cmake_c_flags       = args.c_flags
  cmake_cxx_flags     = args.cxx_flags
  cmake_linker_flags  = ' '.join((args.link_flags,
                                  '-static-libgcc',
                                  '-static-libstdc++',
                                  '-static'))
  verbose             = args.verbose

  llvm_targets_to_build = 'X86'

  os.chdir(build_dir)

  cmake_args = ['cmake.exe']
  cmake_args.append('-G')
  cmake_args.append('Ninja')
  cmake_args.append(source_dir)
  cmake_args.append('-DCMAKE_INSTALL_PREFIX='        + cmake_install_prefix)
  cmake_args.append('-DCMAKE_BUILD_TYPE='            + cmake_build_type)

  if cmake_c_flags:
    cmake_args.append('-DCMAKE_C_FLAGS='             + cmake_c_flags)

  if cmake_cxx_flags:
    cmake_args.append('-DCMAKE_CXX_FLAGS='           + cmake_cxx_flags)

  if cmake_linker_flags:
    cmake_args.append('-DCMAKE_EXE_LINKER_FLAGS='    + cmake_linker_flags)
    cmake_args.append('-DCMAKE_SHARED_LINKER_FLAGS=' + cmake_linker_flags)

  cmake_args.append('-DLLVM_TARGETS_TO_BUILD='       + llvm_targets_to_build)

  ninja_args = ['ninja.exe']

  if verbose:
    ninja_args.append('-v')

  ninja_args.append('install/strip')

  subprocess.call(cmake_args)
  subprocess.call(ninja_args)


if __name__ == '__main__':
  main()
